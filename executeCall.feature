#language: pt
Funcionalidade: Realizar Chamada
 
	Eu como usuário quero poder realizar uma chamada de telefone através do aplicativo

      Cenario: Realizar chamada para um contato da lista
	Dado que estou em um chat com um contato
	Quando seleciono a opcção "Chamar"
    Então o aplicativo abre a tela de "Chamada"
	E o registro da chamada deve ser gravado na aba "Chamadas"

