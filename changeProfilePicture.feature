#language: pt
Funcionalidade: Alterar imagem do meu perfil
  
	Eu como usuário quero poder alterar a imagem do meu perfil para uma imagem da minha galeria
	Uma Vez que já tenha sido alterado quero remover a imagem do perfil

       Cenario: Alterar imagem do perfil
	Dado que estou na tela "Configurações"
	E seleciono a opção "Perfil"
	E seleciono o item "Editar"
	E seleciono a opção "Galeria"
	E seleciono a "Imagem 1"
	Entao o campo deve exibir a "Imagem 1" no meu perfil
	
	
		Cenario: Remover imagem do perfil
	Dado que estou na tela "Configurações"
	E seleciono a opção "Perfil"
	E seleciono o item "Editar"
	E seleciono a opção "Remover foto"
	Então o campo deve exibir a "Imagem padrão"
